package com.edu.cloud.controller;


import com.atguigu.cloud.rspon.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.edu.cloud.entiy.SResume;
import com.edu.cloud.entiy.Users;
import com.edu.cloud.service.SResumeService;
import com.edu.cloud.service.UserService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class RootStudentManageController {
    @Resource
    UserService stuManageService;
    @Resource
    SResumeService sResumeService;
    /**
     * 学生管理列表
     * @return
     */
    @ResponseBody
    @GetMapping("stu/show")
    public CommonResult stuShow(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size){
        QueryWrapper<Users> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("uid","username","image_data","create_time","mobile","email","age","pid").eq("pid", 2);
        IPage<Users> page = stuManageService.page(new Page<>(current, size), queryWrapper);
        if (page.getRecords().isEmpty()) return CommonResult.failed("获取学生列表失败！！");
        return CommonResult.success(page,"获取学生列表成功！！");
    }

    /**
     * 通过ID修改学生信息
     * @return
     */
    @ResponseBody
    @PostMapping("stu/edit")
    public CommonResult stuEditById(@RequestBody Users users){
        users.setCreateTime(null);
        UpdateWrapper<Users> usersUpdateWrapper = new UpdateWrapper<>();
        usersUpdateWrapper.eq("uid",users.getUid());
        usersUpdateWrapper.set("username",users.getUsername());
        usersUpdateWrapper.set("image_data",users.getImageData());
        usersUpdateWrapper.set("mobile",users.getMobile());
        usersUpdateWrapper.set("email",users.getEmail());
        usersUpdateWrapper.set("age",users.getAge());
        usersUpdateWrapper.set("pid",users.getPid());
        boolean update = stuManageService.update(users, usersUpdateWrapper);
        if (!update) return CommonResult.failed("修改失败！！");
        return CommonResult.success(null,"修改成功！！");
    }

    /**
     * 通过ID删除学生信息
     * @return
     */
    @ResponseBody
    @PostMapping("stu/delete")
    public CommonResult stuDeleteById(@RequestBody Users users){
        QueryWrapper<SResume> sResumeQueryWrapper = new QueryWrapper<>();
        sResumeQueryWrapper.eq("uid",users.getUid());
        // 先删除简历
        sResumeService.remove(sResumeQueryWrapper);

        boolean b = stuManageService.removeById(users.getUid());
        if (!b)  return CommonResult.failed("删除失败！！");
        return CommonResult.success(null,"删除成功！！");
    }





}
