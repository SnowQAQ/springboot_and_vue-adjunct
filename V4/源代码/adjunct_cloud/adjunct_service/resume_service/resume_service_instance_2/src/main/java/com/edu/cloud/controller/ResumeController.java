package com.edu.cloud.controller;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson2.annotation.JSONField;
import com.atguigu.cloud.rspon.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.edu.cloud.entiy.NotesManage;
import com.edu.cloud.entiy.Recruitment;
import com.edu.cloud.entiy.SResume;
import com.edu.cloud.service.NotesManageService;
import com.edu.cloud.service.RecruitmentService;
import com.edu.cloud.service.SResumeService;
import com.edu.cloud.vo.RecruitmentAndSonNotesManageVo;
import com.edu.cloud.vo.SResumeAndNid;
import com.edu.cloud.vo.SResumeAndUserVo;
import javax.annotation.Resource;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Controller
public class ResumeController {
    @Resource
    NotesManageService notesManageService;
    @Resource
    RecruitmentService recruitmentService;

    /**
     * 商家点击简历查看需要改变简历状态码 通过nid
     */
    @ResponseBody
    @GetMapping("notes/change/state/{nid}")
    public CommonResult changeState(@PathVariable Integer nid) {
        UpdateWrapper<NotesManage> notesManageUpdateWrapper = new UpdateWrapper<>();
        notesManageUpdateWrapper.eq("n_id",nid);
        notesManageUpdateWrapper.setSql("`state` = 2");
        boolean update = notesManageService.update(notesManageUpdateWrapper);
        if (!update) return CommonResult.failed("更新简历状态失败！");
        return CommonResult.success(null,"更新简历状态成功！");
    }


    /**
     * 简历通过功能
     * 商家添加通知信息、更改状态码为已通过状态
     */
    @ResponseBody
    @PostMapping("managehome/notes/change")
    public CommonResult changeNotesInfoAndState(@RequestBody NotesManage notesManage) {
        System.out.println(notesManage);
        UpdateWrapper<NotesManage> notesManageUpdateWrapper = new UpdateWrapper<>();
        notesManageUpdateWrapper.eq("n_id",notesManage.getNId());
        notesManageUpdateWrapper.set("notes_info",notesManage.getNotesInfo());
        notesManageUpdateWrapper.set("state",notesManage.getState());
        boolean update = notesManageService.update(notesManageUpdateWrapper);
        System.out.println(update);
        if (!update) return CommonResult.failed("简历通知失败！");
        return CommonResult.success(null,"简历通知成功！");
    }


    @Resource
    SResumeService sResumeService;
    /**
     * 管理学生简历
     * @return
     */
    @ResponseBody
    @GetMapping("stu/notes/manage")
    public CommonResult sResumeManage(){
        List<SResumeAndUserVo> stuNotesAndUserInfo = sResumeService.getStuNotesAndUserInfo();
        if (stuNotesAndUserInfo.isEmpty()) return  CommonResult.failed("暂无数据！！");
        return CommonResult.success(stuNotesAndUserInfo,"简历获取成功！！");
    }


    @Value("${ip:localhost}")
    String ip;
    @Value("${server.port}")
    String port;

    //获取项目根目录路径
    //以及文件要存储目录 F:\SpingBoot_Stu\adjunct_cloud +...
    private static final String ROOT_PATH =  System.getProperty("user.dir") + File.separator + "files";
    /**
     * 上传简历
     */
    @ResponseBody
    @PostMapping(value = "feign/stu/notes/upload/{uid}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @JSONField(serialize = false)
    public CommonResult upload(@PathVariable Integer uid,@RequestPart MultipartFile file) throws IOException {
        System.out.println("api"+uid+"   \n"+file.getOriginalFilename());
        //先判断这学生数据库有没有三个简历 最多3个简历
        Integer notesConstById = sResumeService.getNotesConstById(uid);
        if (notesConstById >= 3){
            return CommonResult.failed("最多只能存储三份简历，请删除更换！");
        }

        //获取文件的原始名称
        String originalFilename = file.getOriginalFilename();
        //去除目录和后缀后的文件名
        String baseName = FilenameUtils.getBaseName(originalFilename);
        // 获取文件的后缀
        String extension = FilenameUtils.getExtension(originalFilename);


        //加上自己文件名
        String fileRealPath = ROOT_PATH + File.separator + originalFilename;

        File saveFile = new File(fileRealPath);
        //判断 我存储的这个文件的父级目录存在不
        if (!saveFile.getParentFile().exists()) {
            //不存在就创建这个父级目录
            saveFile.getParentFile().mkdirs();
        }
        //如果传递来的文件名字重复 就要重命名这个文件 时间戳+~
        if (saveFile.exists()) {
            long l = System.currentTimeMillis();
            //再初始化 获取到到的源文件路径名称 fileRealPath
            fileRealPath = ROOT_PATH + File.separator + l + "_" + baseName + "." + extension;
            originalFilename = l + "_" + baseName + "." + extension;
            saveFile = new File(fileRealPath);
        }




        String url = "http://" + ip + ":" + "8888" + "/stu/notes/download/" + originalFilename;


        //最后存入数据库
        SResume sResume = new SResume();
        sResume.setCreateTime(null);
        sResume.setUid(uid);
        sResume.setFilename(originalFilename);
        sResume.setFilepath(url);
        boolean save = sResumeService.save(sResume);

        if (!save) return  CommonResult.failed("存入简历失败！！");
        System.out.println(111111);
        file.transferTo(saveFile);
        System.out.println(2222222);

        return CommonResult.success(url, "上传成功");
    }

    /**
     * 获取简历
     */
    @ResponseBody
    @GetMapping("stu/notes/{uid}")
    public CommonResult getNotes(@PathVariable Integer uid){
        QueryWrapper<SResume> sResumeQueryWrapper = new QueryWrapper<>();
        sResumeQueryWrapper.eq("uid",uid);
        List<SResume> list = sResumeService.list(sResumeQueryWrapper);

        if (list.isEmpty()) return CommonResult.failed("当前无简历，请您添加！！");

        return CommonResult.success(list,"获取简历成功！！");
    }

    /**
     * 下载简历
     */
    @ResponseBody
    @GetMapping("stu/notes/download/{fileName}")
    public CommonResult download(@PathVariable String fileName, HttpServletResponse response) throws IOException {
        String filePath = ROOT_PATH + File.separator + fileName;
        File file = new File(filePath);
        if (!file.exists()){
            return CommonResult.failed("没有这文件！！");
        }
        //读取这个文件
        byte[] bytes = FileUtil.readBytes(file);
        //写出去
        ServletOutputStream outputStream = response.getOutputStream();
        outputStream.write(bytes);
        outputStream.flush();
        outputStream.close();
        return CommonResult.success(bytes,"获取成功，正在下载");
    }



    /**
     * 删除简历
     */
    @ResponseBody
    @PostMapping("stu/notes/del")
    public CommonResult delNotes(@RequestParam(name = "uid") Integer uid,@RequestParam(name = "fileName") String fileName){

        //先删除文件夹的文件 再删除数据库对应的文件
//        File file = new File(ROOT_PATH+File.separator+fileName);
//        System.out.println(file.exists());
//        if (file.exists()) file.delete();


        QueryWrapper<SResume> sResumeQueryWrapper = new QueryWrapper<>();
        //名字已经确保不重复
        sResumeQueryWrapper.eq("uid",uid).eq("filename",fileName);

        boolean remove = sResumeService.remove(sResumeQueryWrapper);
        if (!remove) return CommonResult.failed("删除简历失败！！");
        return CommonResult.success(null,"删除简历成功！！");
    }



    /**
     * 投递简历
     * 直接存储这学生的uid、简历t_id、某个招聘rec_id
     */
    @ResponseBody
    @PostMapping("stu/notes/send")
    public CommonResult sendNotes(@RequestBody NotesManage notesManage){
        System.out.println(notesManage);
        //判断一个学生投了同一个招聘，不给投 返回每份简历只能投一次
        Integer size =  notesManageService.getMyNotesCount(notesManage.getUid(),notesManage.getRecId());
        if (size >= 1) return CommonResult.failed("您已投递简历，请勿重复投递！！");
        boolean save = notesManageService.save(notesManage);
        if (!save) return CommonResult.failed("投递简历失败！！");
        return CommonResult.success(null,"投递简历成功，待商家回复！！");
    }


    /**
     * 通过学生id获取自己的投递信息
     * 返回某个招聘下的投递 以及状态码、通知
     */
    @ResponseBody
    @GetMapping("stu/deliver/{uid}")
    public CommonResult getMyDeliver(@PathVariable Integer uid)  {
        QueryWrapper<NotesManage> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uid",uid);
        //获取学生自己投递的简历
        List<NotesManage> list1 = notesManageService.list(queryWrapper);
        HashMap<Integer, ArrayList<SResumeAndNid>> map = new HashMap<>();
        List<RecruitmentAndSonNotesManageVo> recruitmentAndSonNotesManageVos = new ArrayList<>();

        //遍历所有这老板旗下的收到的简历
        for (NotesManage notesManage : list1) {
            //如果Map集合中recid不存在就创建recid 指向 这招聘信息的所有简历
            if(!map.containsKey(notesManage.getRecId())){
                //获取这次不重复招聘信息 的 学生简历
                SResumeAndNid sResume = sResumeService.getOneTid(notesManage.getTId());
                sResume.setNId(notesManage.getNId());
                sResume.setState(notesManage.getState());
                sResume.setNotesInfo(notesManage.getNotesInfo());
                ArrayList<SResumeAndNid> sResumeLists = new ArrayList<>();
                sResumeLists.add(sResume);
                //存放到map中
                map.put(notesManage.getRecId(),sResumeLists);
            }else {
                //如果map中已经存在这简历了就要获取这map的学生简历，然后追加新的学生简历，再重置这集合
                ArrayList<SResumeAndNid> sResumes = map.get(notesManage.getRecId());
                ArrayList<SResumeAndNid> newsResumes = new ArrayList<>();
                SResumeAndNid sResume = sResumeService.getOneTid(notesManage.getTId());
                sResume.setNId(notesManage.getNId());
                sResume.setState(notesManage.getState());
                sResume.setNotesInfo(notesManage.getNotesInfo());
                newsResumes.addAll(sResumes);
                newsResumes.add(sResume);
                map.put(notesManage.getRecId(),newsResumes);
            }
        }

        RecruitmentAndSonNotesManageVo recruitmentAndSonNotesManageVo = null;
        for (Object o : map.keySet().toArray()) {
            recruitmentAndSonNotesManageVo = new RecruitmentAndSonNotesManageVo();
            QueryWrapper<Recruitment> recruitmentQueryWrapper = new QueryWrapper<>();
            recruitmentQueryWrapper.eq("rec_id",(Integer) o);
            final Recruitment byId = recruitmentService.getOne(recruitmentQueryWrapper);
            recruitmentAndSonNotesManageVo.setRecSalary(byId.getRecSalary());
            recruitmentAndSonNotesManageVo.setRecInfo(byId.getRecInfo());
            recruitmentAndSonNotesManageVo.setRecId(byId.getRecId());
            recruitmentAndSonNotesManageVo.setRecHeat(byId.getRecHeat());
            recruitmentAndSonNotesManageVo.setRecObj(byId.getRecObj());

            recruitmentAndSonNotesManageVo.setRecStation(byId.getRecStation());

            ArrayList<SResumeAndNid> sResumes = new ArrayList<>();
            sResumes.addAll(map.get(o));
            recruitmentAndSonNotesManageVo.setChildren(sResumes);

            recruitmentAndSonNotesManageVos.add(recruitmentAndSonNotesManageVo);
        }
        return CommonResult.success(recruitmentAndSonNotesManageVos,"....");
    }
}
