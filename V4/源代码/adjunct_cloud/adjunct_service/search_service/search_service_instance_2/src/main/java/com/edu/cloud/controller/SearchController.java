package com.edu.cloud.controller;


import com.atguigu.cloud.rspon.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.edu.cloud.entiy.NotesManage;
import com.edu.cloud.entiy.Recruitment;
import com.edu.cloud.entiy.Users;
import com.edu.cloud.service.*;
import com.edu.cloud.vo.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Controller
public class SearchController {
    @Resource
    UserService stuManageService;
    @Resource
    UserService merchantsUserService;
    @Resource
    RecruitmentService recruitmentService;
    @Resource
    BusinessShopService businessShopService;

    @Resource
    NotesManageService notesManageService;
    @Resource
    SResumeService sResumeService;
    /**
     * 通过用户名搜索数据
     * @return
     */
    @ResponseBody
    @GetMapping("stu/search")
    public CommonResult stuSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search){
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        //搜索返回指定字段 然后pid要是学生的
        wrapper.select("uid", "username", "image_data", "create_time", "mobile", "email", "age","pid").like("username", search).eq("pid", 2);
        IPage<Users> list = stuManageService.page(new Page<>(current,size),wrapper);
        if (list.getRecords().isEmpty()) return CommonResult.failed("搜索失败！！");
        return CommonResult.success(list,"搜索成功！！");
    }

    /**
     * 商家用户名搜索
     */
    @ResponseBody
    @GetMapping("business/search")
    public CommonResult merchantsSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search) {
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.select("uid", "username", "image_data", "create_time", "mobile", "email", "age", "pid").like("username", search).eq("pid", 3);
        IPage<Users> list = merchantsUserService.page(new Page<>(current, size), wrapper);
        if (list.getRecords().isEmpty()) return CommonResult.failed("搜索失败！！");
        return CommonResult.success(list, "搜索成功！！");
    }


    /**
     * 商店名字搜索
     */
    @ResponseBody
    @GetMapping("shop/search")
    public CommonResult shopSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search) {
        Page<BusinessShopAllInfoVo> page = new Page<>(current, size);
        IPage<BusinessShopAllInfoVo> businessShopInfoByLikeName = businessShopService.getBusinessShopInfoByLikeName(page, "%" + search + "%");

        for (BusinessShopAllInfoVo businessShopAllInfoVo : businessShopInfoByLikeName.getRecords()) {
            QueryWrapper<Recruitment> recruitmentQueryWrapper = new QueryWrapper<>();
            Integer bId = businessShopAllInfoVo.getBId();
            recruitmentQueryWrapper.eq("b_id", bId);
            //这商店的所有招聘
            List<Recruitment> list = recruitmentService.list(recruitmentQueryWrapper);
            List<RecruitmentAndSonNotesManageVo> recruitmentAndSonNotesManageVos = new ArrayList<>();
            //遍历招聘获取 每个招聘收到的简历
            for (Recruitment o : list) {

                final RecruitmentAndSonNotesManageVo recruitmentAndSonNotesManageVo = new RecruitmentAndSonNotesManageVo();
                recruitmentAndSonNotesManageVo.setRecStation(o.getRecStation());
                recruitmentAndSonNotesManageVo.setRecId(o.getRecId());
                recruitmentAndSonNotesManageVo.setRecHeat(o.getRecHeat());
                recruitmentAndSonNotesManageVo.setRecObj(o.getRecObj());
                recruitmentAndSonNotesManageVo.setRecSalary(o.getRecSalary());
                recruitmentAndSonNotesManageVo.setRecInfo(o.getRecInfo());
                QueryWrapper<NotesManage> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("rec_id", o.getRecId());
                //一系列简历
                List<NotesManage> list1 = notesManageService.list(queryWrapper);
                List<SResumeAndNid> sResumes  = new ArrayList<>();;
                for (NotesManage notesManage : list1) {
                    List<SResumeAndNid> allNotesByTid = sResumeService.getAllNotesByTid(notesManage.getTId(),notesManage.getNId());
                    //获取到所有的这个招聘的简历，然后存储
                    sResumes.addAll(allNotesByTid);
                }
                recruitmentAndSonNotesManageVo.setChildren(sResumes);
                recruitmentAndSonNotesManageVos.add(recruitmentAndSonNotesManageVo);
            }
            businessShopAllInfoVo.setChildren(recruitmentAndSonNotesManageVos);
        }

        if (businessShopInfoByLikeName.getRecords().isEmpty()) return CommonResult.failed("获取商家列表失败！！");
        return CommonResult.success(businessShopInfoByLikeName, "获取商家列表成功！！");
    }


    /**
     * 搜索职位关键字返回前端 所有信息
     */
    @ResponseBody
    @GetMapping("suthome/stuindex/search/{search}")
    public CommonResult searchRecInfo(@PathVariable String search) {
        List<RecruitmentAllInfoVo> userAllInfoSearch = recruitmentService.getUserAllInfoSearch("%" + search + "%");
        if (userAllInfoSearch.isEmpty()) return CommonResult.failed("未搜索到关键字信息！");
        return CommonResult.success(userAllInfoSearch, "搜索成功！");
    }

}
