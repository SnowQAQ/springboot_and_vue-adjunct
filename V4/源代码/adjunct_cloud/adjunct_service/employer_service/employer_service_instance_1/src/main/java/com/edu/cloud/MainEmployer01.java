package com.edu.cloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient //该注解用于向使用consul为注册中心时注册服务

@RefreshScope // 动态刷新
public class MainEmployer01 {
    public static void main(String[] args) {
        SpringApplication.run(MainEmployer01.class,args);

    }
}