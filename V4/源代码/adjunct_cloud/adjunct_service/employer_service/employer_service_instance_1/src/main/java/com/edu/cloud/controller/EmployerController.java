package com.edu.cloud.controller;


import com.atguigu.cloud.rspon.CommonResult;
import com.atguigu.cloud.rspon.ReturnCode;
import com.edu.cloud.entiy.*;
import com.edu.cloud.service.BusinessShopService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@Controller
public class EmployerController {
    String defaultImgUser = ReturnCode.DEFAULT_IMG_USER;

    @Resource
     BusinessShopService shopService;


    /**
     * 通过商家id获取店铺的信息列表
     */
    @ResponseBody
    @GetMapping("managehome/shop/{id}")
    public CommonResult shopShow(@PathVariable Integer id) {
        List list = shopService.shopShow(id);

        if (list.size() == 0) {
            return CommonResult.failed("该商家没有店铺信息，请添加！");
        }

        return CommonResult.success(list, "获取店铺列表成功！");
    }

    /**
     * 添加商店信息
     */
    @ResponseBody
    @PostMapping("managehome/shop/add")
    public CommonResult shopAdd(@RequestBody BusinessShop businessShop) throws IOException {
        boolean save = shopService.shopAdd(businessShop);
        if (save) return CommonResult.success(null,"添加成功！");
        return CommonResult.failed("添加失败！");
    }


    /**
     * 修改商店信息
     */
    @ResponseBody
    @PostMapping("managehome/shop/edit")
    public CommonResult shopEdit(@RequestBody BusinessShop businessShop) {
        if (businessShop == null) CommonResult.failed("添加失败！");

        boolean  update = shopService.shopEdit(businessShop);

        if (!update) return CommonResult.failed("店铺信息更新失败！");
        return CommonResult.success(null,"店铺信息更新成功！");
    }


    /**
     * 删除商店
     */
    @ResponseBody
    @GetMapping("managehome/shop/delete/{id}")
    public CommonResult shopDelete(@PathVariable Integer id) {
        boolean remove = shopService.shopDelete(id);
        if (!remove) CommonResult.failed("删除店铺失败！");
        return CommonResult.success(null,"删除店铺成功！");
    }
}
