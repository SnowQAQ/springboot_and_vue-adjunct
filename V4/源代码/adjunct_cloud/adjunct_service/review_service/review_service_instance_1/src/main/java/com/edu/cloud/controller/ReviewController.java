package com.edu.cloud.controller;
import javax.annotation.Resource;

import com.atguigu.cloud.rspon.CommonResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.edu.cloud.entiy.*;
import com.edu.cloud.service.*;
import com.edu.cloud.vo.CommentAndChildrenVo;
import com.edu.cloud.vo.CommentReplyAndUserInfoVo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class ReviewController {

    @Resource
    CommentService commentService;
    @Resource
    CommentReplyService commentReplyService;

    /**
     * 通过简历id展现出对应的评论
     */
    @ResponseBody
    @GetMapping("stu/rec/comment/{rid}")
    public CommonResult commentShowByRecId(@PathVariable Integer rid)  {
        QueryWrapper<CommentAndChildrenVo> commentQueryWrapper = new QueryWrapper<>();
        commentQueryWrapper.eq("rec_id",rid);

        List<CommentAndChildrenVo> commentByRid = commentService.getCommentByRid(rid);
        if (commentByRid.isEmpty()) return CommonResult.failed("当前简历无评论");
        for (CommentAndChildrenVo commentAndChildrenVo : commentByRid) {
            //获取某个父级评论的所有子评论 存入
            List<CommentReplyAndUserInfoVo> commentReplyByCid = commentService.getCommentReplyByCid(commentAndChildrenVo.getComId());
            commentAndChildrenVo.setChildren(commentReplyByCid);
        }
        //返回前端
        return CommonResult.success(commentByRid,"获取评论成功");
    }

    /**
     * 增加一级评论
     */
    @ResponseBody
    @PostMapping("stu/rec/add/comment")
    public CommonResult addComment(@RequestBody Comment comment)  {
        comment.setCreateTime(null);
        comment.setComId(null);
        boolean save = commentService.save(comment);
        if (!save)  return CommonResult.failed("添加评论失败");
        return CommonResult.success(null,"添加评论成功");
    }


    /**
     * 增加二级评论 前端把com_id存入answer_id 外键
     */
    @ResponseBody
    @PostMapping("stu/rec/add/reply")
    public CommonResult addCommentReply(@RequestBody CommentReply comment)  {
        System.out.println(comment);
        comment.setCreateTime(null);
        comment.setRepId(null);
        boolean save = commentReplyService.save(comment);
        if (!save)  return CommonResult.failed("添加评论失败");
        return CommonResult.success(null,"添加评论成功");
    }

    /**
     * 删除指定父级评论
     */
    @ResponseBody
    @GetMapping("stu/rec/del/comment/{cid}")
    public CommonResult delComment(@PathVariable Integer cid)  {
        //先删除 子评论 再删除父级评论
        QueryWrapper<CommentReply> commentReplyQueryWrapper = new QueryWrapper<>();
        commentReplyQueryWrapper.eq("answer_id",cid);
        commentReplyService.remove(commentReplyQueryWrapper);

        QueryWrapper<Comment> commentQueryWrapper = new QueryWrapper<>();
        commentQueryWrapper.eq("com_id",cid);

        boolean remove = commentService.remove(commentQueryWrapper);
        if (!remove)  return CommonResult.failed("删除评论失败");

        return CommonResult.success(null,"删除评论成功");
    }


    /**
     * 删除指定子级评论
     */
    @ResponseBody
    @GetMapping("stu/rec/del/reply/{rid}")
    public CommonResult delCommentReply(@PathVariable Integer rid)  {
        //先删除 子评论 再删除父级评论
        QueryWrapper<CommentReply> commentReplyQueryWrapper = new QueryWrapper<>();
        commentReplyQueryWrapper.eq("rep_id",rid);
        boolean remove = commentReplyService.remove(commentReplyQueryWrapper);
        if (!remove)  return CommonResult.failed("删除评论失败");

        return CommonResult.success(null,"删除评论成功");
    }

}
