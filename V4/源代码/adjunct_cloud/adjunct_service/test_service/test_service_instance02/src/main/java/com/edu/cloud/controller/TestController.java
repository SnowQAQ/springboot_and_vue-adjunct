package com.edu.cloud.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.Utilities;
import java.util.concurrent.TimeUnit;


@Controller
public class TestController {
    //测试限速 和 断路器
    @GetMapping(value = "test/circuit/{id}")
    @ResponseBody
    public String myCircuit(@PathVariable("id") Integer id)
    {
        if(id == -4) throw new RuntimeException("----circuit id 不能负数");
        if(id == 9999){
            try { TimeUnit.SECONDS.sleep(5); } catch (InterruptedException e) { e.printStackTrace(); }
        }
        return "Hello, circuit! inputId:  "+id + "这是二号微服务";
    }
}


