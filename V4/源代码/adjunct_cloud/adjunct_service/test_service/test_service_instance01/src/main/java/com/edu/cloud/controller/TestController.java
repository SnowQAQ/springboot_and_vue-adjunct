package com.edu.cloud.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.Utilities;
import java.util.concurrent.TimeUnit;

/**
 *     SUCCESS(200, "操作成功"),
 *     FAILED(500, "操作失败"),
 *     VALIDATE_FAILED(404, "参数检验失败"),
 *     UNAUTHORIZED(401, "暂未登录或token已过期"),
 *     FORBIDDEN(403, "没有相关权限");
 */
@Controller
public class TestController {
    //测试限速 和 断路器
    @GetMapping(value = "test/circuit/{id}")
    @ResponseBody
    public String myCircuit(@PathVariable("id") Integer id)
    {
        if(id == -4) throw new RuntimeException("----circuit id 不能负数");
        if(id == 9999){
            try { TimeUnit.SECONDS.sleep(5); } catch (InterruptedException e) { e.printStackTrace(); }
        }
        return "Hello, circuit! inputId:  "+id + "这是一号微服务";
    }
}


