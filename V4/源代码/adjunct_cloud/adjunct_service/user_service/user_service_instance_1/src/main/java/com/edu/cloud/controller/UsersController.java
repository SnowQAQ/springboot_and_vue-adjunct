package com.edu.cloud.controller;


import com.edu.cloud.config.TokenUtil;
import com.atguigu.cloud.rspon.CommonResult;
import com.atguigu.cloud.rspon.ReturnCode;
import com.atguigu.cloud.tool.Base64Utils;
import com.atguigu.cloud.tool.MD5Utils;
import com.atguigu.cloud.tool.UUIDUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.edu.cloud.entiy.Users;
import com.edu.cloud.service.UserService;
import javax.annotation.Resource;

import com.edu.cloud.vo.EditPasswordVo;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Objects;

/**
 *     SUCCESS(200, "操作成功"),
 *     FAILED(500, "操作失败"),
 *     VALIDATE_FAILED(404, "参数检验失败"),
 *     UNAUTHORIZED(401, "暂未登录或token已过期"),
 *     FORBIDDEN(403, "没有相关权限");
 */
@Controller
public class UsersController {
    String defaultImgUser = ReturnCode.DEFAULT_IMG_USER;

    /**
     * 登录功能
     */
    @Resource
    private UserService userService,stuUserService;

    @PostMapping("login")
    @ResponseBody
    public CommonResult login(@RequestBody Users user) {
//        测试重试机制
//        System.out.println("test");
//        TimeUnit.SECONDS.sleep(60);

//        测试断路器
//        System.out.println("test");
//        System.out.println(4/0);
        QueryWrapper<Users> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(user.getUsername() != null, "username", user.getUsername());
        Users users = userService.getOne(queryWrapper);
        if (users == null) return CommonResult.failed("账号密码有误！！");
        // 密码校验
        String s = (MD5Utils.md5(user.getPassword()+users.getSalt()));
        if (users.getPassword().equals(s)==false){
//            new OutputObject(ReturnCode.FAIL,"密码不正确",user);
            return CommonResult.failed("账号密码有误！！");
        }
        queryWrapper.in(user.getPassword() != null, "password", s);
        // 通过用户名从数据库中查询出该用户
        if (users == null){
//            OutputObject(ReturnCode.FAIL,"用户不存在",user);
            return CommonResult.failed("用户不存在！！");
        }
        String token = TokenUtil.sign(new Users(user.getUsername(),s));
        HashMap<String,Object> hs =new HashMap<>();
        hs.put("uid",users.getUid());
        hs.put("username",users.getUsername());
        hs.put("mobile",users.getMobile());
        hs.put("email",users.getEmail());
        hs.put("age",users.getAge());
        hs.put("token",token);
        hs.put("imageData",users.getImageData());
        hs.put("pid",users.getPid());


//        OutputObject(String.valueOf(HttpStatus.OK.value()),"成功",hs);
        return CommonResult.success(hs,"登录成功！！");

    }


    /**
     * 用户注册
     * @param user
     * @return
     */
    @PostMapping("register")
    @ResponseBody
    public CommonResult addUser(@RequestBody Users user) {
        try {
            // 查询用户名是否存在
            QueryWrapper<Users> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("username",user.getUsername());
            Users users = userService.getOne(queryWrapper);
            if (users!=null){
                return CommonResult.failed("该用户已存在");
            }
            // 设置盐
            String salt = UUIDUtils.getUUID();
            user.setSalt(salt);
            // 设置密码加密
            String s = MD5Utils.md5(user.getPassword()+salt);
            // 设置用户默认头像
            user.setPassword(s);
            ClassPathResource readFile = new ClassPathResource(defaultImgUser);
            File file = readFile.getFile();
            MultipartFile cMultiFile = new MockMultipartFile(file.getName(), file.getName(), null, new FileInputStream(file));
            String base64img = Base64Utils.convertToBase64(cMultiFile);
            user.setImageData(base64img);
            userService.save(user);
            return CommonResult.success("注册成功");
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResult.failed("注册失败");
        }
    }


    /**
     * 个人中心
     */
    @ResponseBody
    @GetMapping("suthome/stuindex/myinfo")
    public CommonResult getInfo(@RequestParam(name = "uid") Integer uid) {
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("uid", uid).select("uid", "username", "image_data", "create_time", "mobile", "email", "age", "pid");
        Users list = stuUserService.getOne(wrapper);

        if (list == null) return CommonResult.failed("未获取到个人数据！");
        return CommonResult.success(list, "获取成功!");
    }

    /**
     * 通过ID修改学生头像
     *
     * @return
     */
    @ResponseBody
    @PostMapping("stu/editimg")
    public CommonResult stuEditImg(@RequestBody Users users) {
        UpdateWrapper<Users> usersUpdateWrapper = new UpdateWrapper<>();
        usersUpdateWrapper.eq("uid", users.getUid());
        usersUpdateWrapper.set("image_data", users.getImageData());
        boolean update = stuUserService.update(users, usersUpdateWrapper);
        if (!update) return CommonResult.failed("修改失败！！");
        return CommonResult.success(null, "修改成功！！");
    }

    /**
     * 修改密码
     */
    @ResponseBody
    @PostMapping("stu/edit/password")
    public CommonResult stuEditPassword(@RequestBody EditPasswordVo editPasswordVo) {
        //判断传递来的是否一样
        if (Objects.equals(editPasswordVo.getOldPassword(), editPasswordVo.getPassword()))
            return CommonResult.failed("密码不能一致！！");
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        //通过uid得到原本的盐
        wrapper.eq("uid", editPasswordVo.getUid());
        String salt = stuUserService.getOne(wrapper).getSalt();

        //原本加密手段之后去对比
        String s = MD5Utils.md5(editPasswordVo.getOldPassword() + salt);
        wrapper.eq("password", s);
        Users one = stuUserService.getOne(wrapper);
        //如果对比错误就不一样
        if (one == null) return CommonResult.failed("旧密码错误！！");
        //对比成功就初始化盐
        salt = UUIDUtils.getUUID();

        UpdateWrapper<Users> usersUpdateWrapper = new UpdateWrapper<>();
        //新的密码 加密
        String newPass = MD5Utils.md5(editPasswordVo.getPassword() + salt);
        usersUpdateWrapper.eq("uid", editPasswordVo.getUid());
        //存新盐
        usersUpdateWrapper.set("salt", salt);
        //新密码
        usersUpdateWrapper.set("password", newPass);
        boolean update = stuUserService.update(usersUpdateWrapper);
        if (!update) return CommonResult.failed("修改密码失败！！");
        return CommonResult.success(null, "修改密码成功！！");
    }

}


