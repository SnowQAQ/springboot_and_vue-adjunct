package com.atguigu.cloud.rspon;

public enum ResultCode implements IErrorCode{
    SUCCESS(200, "操作成功"),
    FAILED(500, "操作失败"),
    VALIDATE_FAILED(404, "参数检验失败"),
    UNAUTHORIZED(401, "暂未登录或token已过期"),
    FORBIDDEN(403, "没有相关权限"),
    BODY_NOT_MATCH(4000,"请求的数据格式不符!"),
    SIGNATURE_NOT_MATCH(4001,"请求的数字签名不匹配!"),
    NOT_FOUND(4004, "未找到该资源!"),
    INTERNAL_SERVER_ERROR(5000, "服务器内部错误!"),
    SERVER_BUSY(5003,"服务器正忙，请稍后再试!"),
    SERVICE_DEGRADATION(5004,"断路器打开！系统繁忙，请稍后再试-----/(ㄒoㄒ)/~~"),
    SERVICE_FLOW_RESTRICTION(5005,"你被限流了！禁止访问/(ㄒoㄒ)/~~");

    /** 定义状态码 */
    private long code;

    /** 定义返回信息 */
    private String message;

    ResultCode() {
    }

    ResultCode(long code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}