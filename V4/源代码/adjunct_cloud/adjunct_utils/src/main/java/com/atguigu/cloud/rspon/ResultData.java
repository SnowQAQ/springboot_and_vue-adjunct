package com.atguigu.cloud.rspon;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
//set返回对象
@Accessors(chain = true)
@AllArgsConstructor
public class ResultData<T> {
    private String code;
    private String message;
    private T data;
    private long timestamp;

    //每次获取最新时间
    ResultData(){
        this.timestamp = System.currentTimeMillis();
    }

    //成功
    public static <T> ResultData<T> success(T data){
        ResultData<T> resultData = new ResultData<T>();
        resultData.setData(data);
        resultData.setCode(ReturnCodeEnum.RC200.getCode());
        resultData.setMessage(ReturnCodeEnum.RC200.getMessage());
        return resultData;
    }

    //失败，通过全局异常获取相对应的异常
    public static <T> ResultData<T> fail(String code , String message){
        ResultData<T> resultData = new ResultData<T>();
        resultData.setCode(code);
        resultData.setMessage(message);
        resultData.setData(null);
        return resultData;
    }
}
