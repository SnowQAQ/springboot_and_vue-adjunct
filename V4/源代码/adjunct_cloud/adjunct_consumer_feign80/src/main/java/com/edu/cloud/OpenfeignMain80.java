package com.edu.cloud;

import com.zaxxer.hikari.HikariConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableDiscoveryClient//该注解用于向使用consul为注册中心时注册服务
@RefreshScope // 动态刷新
@EnableFeignClients
public class OpenfeignMain80 {
    public static void main(String[] args) {
        SpringApplication.run(OpenfeignMain80.class,args);
    }
}