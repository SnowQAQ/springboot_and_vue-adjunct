package com.edu.cloud.controller;

import javax.annotation.Resource;

import com.atguigu.cloud.rspon.CommonResult;
import com.edu.cloud.apis.*;
import com.edu.cloud.entiy.*;
import com.edu.cloud.vo.*;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;

@RestController
public class ConsumerController {
    @Resource
    private EmpFeignApi empFeignApi;
    @Resource
    private JobFeignApi jobFeignApi;
    @Resource
    private ResumeFeignApi resumeFeignApi;
    @Resource
    private ReviewFeignApi reviewFeignApi;
    @Resource
    private RootEmployerFeignApi rootEmployerFeignApi;
    @Resource
    private RootStudentsFeignApi rootStudentsFeignApi;
    @Resource
    private SearchFeignApi searchFeignApi;
    @Resource
    private UserFeignApi userFeignApi;
    @Resource
    private TestFeignApi testFeignApi;


    @CircuitBreaker(name = "employer-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/managehome/shop/{id}")
    @RateLimiter(name = "employer-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult shopShow(@PathVariable Integer id) {
        return empFeignApi.shopShow(id);

    }

    /**
     * 添加商店信息
     */
    @CircuitBreaker(name = "employer-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/managehome/shop/add")
    @RateLimiter(name = "employer-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult shopAdd(@RequestBody BusinessShop businessShop) throws IOException {
        System.out.println(businessShop);
        return empFeignApi.shopAdd(businessShop);

    }


    /**
     * 修改商店信息
     */
    @CircuitBreaker(name = "employer-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/managehome/shop/edit")
    @RateLimiter(name = "employer-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult shopEdit(@RequestBody BusinessShop businessShop) {
        return empFeignApi.shopEdit(businessShop);

    }


    /**
     * 删除商店
     */
    @CircuitBreaker(name = "employer-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/managehome/shop/delete/{id}")
    @RateLimiter(name = "employer-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult shopDelete(@PathVariable Integer id) {
        return empFeignApi.shopDelete(id);

    }


    /**
     * 通过店铺id获取招聘信息
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/managehome/shop/recruitment/{id}")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult shopRecruitmentShow(@PathVariable Integer id) {
        return jobFeignApi.shopRecruitmentShow(id);

    }

    /**
     * 新增招聘信息
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/managehome/shop/recruitment/add")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult addRecruitment(@RequestBody Recruitment recruitment) {
        return jobFeignApi.addRecruitment(recruitment);

    }

    /**
     * 修改招聘信息
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/managehome/shop/recruitment/edit")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult editRecruitment(@RequestBody Recruitment recruitment) {
        return jobFeignApi.editRecruitment(recruitment);

    }


    /**
     * 删除招聘信息 recId
     * 删除他的头条 和 轮播图 和 收到的简历、评论 再删除这招聘
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/managehome/shop/recruitment/del/{id}")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult delRecruitmentShow(@PathVariable Integer id) {
        return jobFeignApi.delRecruitmentShow(id);

    }

    /**
     * 海报标题操作
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/recruitment/poster/exist/{rid}")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult RecruitmentPosterExist(@PathVariable Integer rid) {
        return jobFeignApi.RecruitmentPosterExist(rid);

    }


    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/recruitment/poster/add")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult RecruitmentPoster(@RequestBody Banner banner) {
        return jobFeignApi.RecruitmentPoster(banner);

    }


    /**
     * 商家获取学生投递的简历 nId uid tId recId
     * 1.商家通过招聘信息来划分
     * 2.查看所有我的bossid 获取到我的所有招聘recid，查看这个招聘有没有学生发简历过来
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/managehome/rec/notes/{id}")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult getMyAllNotes(@PathVariable Integer id) {
        return jobFeignApi.getMyAllNotes(id);

    }


    /**
     * 获取所有简历 以及 主人-商店详细
     *
     * @return
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/suthome/stuindex/getrec")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult getRec() {
        return jobFeignApi.getRec();

    }

    /**
     * 通过简历rid获取所有这个简历 主人-商店详细信息
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/suthome/stuindex/recshow/{id}")
    public CommonResult getRecInfoByid(@PathVariable Integer id) {
        System.out.println(id);
        return jobFeignApi.getRecInfoByid(id);

    }

    /**
     * 前端点击访问某个招聘页面，就给这个招聘信息热度+1
     */

    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/suthome/stuindex/addheat/{rid}")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult AddRecHeat(@PathVariable Integer rid) {
        return jobFeignApi.AddRecHeat(rid);

    }

    /**
     * 前端轮播图 头条数据获取
     */
    @CircuitBreaker(name = "job-management-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/suthome/stuindex/bannertop")
    @RateLimiter(name = "job-management-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult getTop() {
        return jobFeignApi.getTop();

    }


    /**
     * 商家点击简历查看需要改变简历状态码 通过nid
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/notes/change/state/{nid}")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult changeState(@PathVariable Integer nid) {
        return resumeFeignApi.changeState(nid);

    }

    /**
     * 简历通过功能
     * 商家添加通知信息、更改状态码为已通过状态
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/managehome/notes/change")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult changeNotesInfoAndState(@RequestBody NotesManage notesManage) {
        return resumeFeignApi.changeNotesInfoAndState(notesManage);

    }

    /**
     * 管理学生简历
     *
     * @return
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/stu/notes/manage")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult sResumeManage() {
        return resumeFeignApi.sResumeManage();

    }

    /**
     * 上传简历
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping(value = "feign/stu/notes/upload/{uid}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult upload(@PathVariable Integer uid, @RequestPart MultipartFile file) throws IOException {
        return resumeFeignApi.upload(uid, file);

    }

    /**
     * 获取简历
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/stu/notes/{uid}")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult getNotes(@PathVariable Integer uid) {
        return resumeFeignApi.getNotes(uid);

    }

    /**
     * 下载简历
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/stu/notes/download/{fileName}")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult download(@PathVariable String fileName, HttpServletResponse response) throws IOException {
        return resumeFeignApi.download(fileName, response);

    }


    /**
     * 删除简历
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/stu/notes/del")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult delNotes(@RequestParam(name = "uid") Integer uid, @RequestParam(name = "fileName") String fileName) {
        return resumeFeignApi.delNotes(uid, fileName);

    }


    /**
     * 投递简历
     * 直接存储这学生的uid、简历t_id、某个招聘rec_id
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    @PostMapping("feign/stu/notes/send")
    public CommonResult sendNotes(@RequestBody NotesManage notesManage) {
        return resumeFeignApi.sendNotes(notesManage);

    }

    /**
     * 通过学生id获取自己的投递信息
     * 返回某个招聘下的投递 以及状态码、通知
     */
    @CircuitBreaker(name = "resume-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/stu/deliver/{uid}")
    @RateLimiter(name = "resume-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult getMyDeliver(@PathVariable Integer uid) {
        return resumeFeignApi.getMyDeliver(uid);

    }


    /**
     * 通过简历id展现出对应的评论
     */
    @CircuitBreaker(name = "review-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/stu/rec/comment/{rid}")
    @RateLimiter(name = "review-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult commentShowByRecId(@PathVariable Integer rid) {
        return reviewFeignApi.commentShowByRecId(rid);

    }

    /**
     * 增加一级评论
     */
    @CircuitBreaker(name = "review-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/stu/rec/add/comment")
    @RateLimiter(name = "review-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult addComment(@RequestBody Comment comment) {
        return reviewFeignApi.addComment(comment);

    }


    /**
     * 增加二级评论 前端把com_id存入answer_id 外键
     */
    @CircuitBreaker(name = "review-service-instance", fallbackMethod = "myCircuitFallback")
    @PostMapping("feign/stu/rec/add/reply")
    @RateLimiter(name = "review-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult addCommentReply(@RequestBody CommentReply comment) {
        return reviewFeignApi.addCommentReply(comment);

    }

    /**
     * 删除指定父级评论
     */
    @CircuitBreaker(name = "review-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/stu/rec/del/comment/{cid}")
    @RateLimiter(name = "review-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult delComment(@PathVariable Integer cid) {
        return reviewFeignApi.delComment(cid);

    }

    /**
     * 删除指定子级评论 review-service-instance
     */
    @CircuitBreaker(name = "review-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/stu/rec/del/reply/{rid}")
    @RateLimiter(name = "review-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult delCommentReply(@PathVariable Integer rid) {
        return reviewFeignApi.delCommentReply(rid);

    }


    /**
     * 商家管理列表
     *
     * @return
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/business/show")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult merchantsShow(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size) {
        return rootEmployerFeignApi.merchantsShow(current, size);

    }

    /**
     * 商店管理列表
     *
     * @return
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @GetMapping("feign/shop/show")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult shopShow(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size) {
        return rootEmployerFeignApi.shopShow(current, size);

    }


    /**
     * 招聘管理列表
     *
     * @return
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/recruitment/show")
    public CommonResult recruitmentShow() {
        return rootEmployerFeignApi.recruitmentShow();

    }


    /**
     * 通过ID修改商家信息
     *
     * @return
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    @PostMapping("feign/business/edit")
    public CommonResult merchantsEditById(@RequestBody Users users) {
        return rootEmployerFeignApi.merchantsEditById(users);

    }

    /**
     * 通过ID删除商家信息
     * 删除这商家的商店、招聘、简历管理、轮播图、头条、评价
     *
     * @return
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/business/del/{uid}")
    public CommonResult merchantsDeleteById(@PathVariable Integer uid) {
        return rootEmployerFeignApi.merchantsDeleteById(uid);

    }


    /**
     * 通过ID修改商店信息
     *
     * @return
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    @PostMapping("feign/shop/edit")
    public CommonResult shopEditById(@RequestBody BusinessShopAllInfoVo businessShop) {
        return rootEmployerFeignApi.shopEditById(businessShop);

    }

    /**
     * 通过ID删除商店信息
     *
     * @return
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/shop/del/{bid}")
    public CommonResult shopDeleteById(@PathVariable Integer bid) {
        return rootEmployerFeignApi.shopDeleteById(bid);

    }

    /**
     * 管理员通过nid删除某个招聘下的某个简历
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    @PostMapping("feign/rec/notes/del")
    public CommonResult delNotesByNid(@RequestParam(name = "nId") Integer nId) {
        return rootEmployerFeignApi.delNotesByNid(nId);

    }


    /**
     * 获取所有招聘信息下的评论信息
     */
    @CircuitBreaker(name = "root-employer-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-employer-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/rec/comment")
    public CommonResult getAllRecAndComment() {
        return rootEmployerFeignApi.getAllRecAndComment();

    }


    /**
     * 学生管理列表
     *
     * @return
     */
    @CircuitBreaker(name = "root-students-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-students-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/stu/show")
    public CommonResult stuShow(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size) {
        return rootStudentsFeignApi.stuShow(current, size);

    }

    /**
     * 通过ID修改学生信息
     *
     * @return
     */
    @CircuitBreaker(name = "root-students-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-students-service-instance", fallbackMethod = "myRatelimitFallback")
    @PostMapping("feign/stu/edit")
    public CommonResult stuEditById(@RequestBody Users users) {
        return rootStudentsFeignApi.stuEditById(users);

    }

    /**
     * 通过ID删除学生信息
     * root-students-service-instance
     *
     * @return
     */
    @CircuitBreaker(name = "root-students-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "root-students-service-instance", fallbackMethod = "myRatelimitFallback")
    @PostMapping("feign/stu/delete")
    public CommonResult stuDeleteById(@RequestBody Users users) {
        return rootStudentsFeignApi.stuDeleteById(users);

    }


    /**
     * 通过用户名搜索数据
     *
     * @return
     */

    @CircuitBreaker(name = "search-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "search-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/stu/search")
    public CommonResult stuSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search) {
        return searchFeignApi.stuSearchByName(current, size, search);

    }

    /**
     * 商家用户名搜索
     */

    @CircuitBreaker(name = "search-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "search-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/business/search")
    public CommonResult merchantsSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search) {
        return searchFeignApi.merchantsSearchByName(current, size, search);

    }


    /**
     * 商店名字搜索
     */

    @CircuitBreaker(name = "search-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "search-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping("feign/shop/search")
    public CommonResult shopSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search) {
        return searchFeignApi.shopSearchByName(current, size, search);

    }

    /**
     * 搜索职位关键字返回前端 所有信息
     */

    @CircuitBreaker(name = "search-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "search-service-instance", fallbackMethod = "myRatelimitFallback")

    @GetMapping("feign/suthome/stuindex/search/{search}")
    public CommonResult searchRecInfo(@PathVariable String search) {
        return searchFeignApi.searchRecInfo(search);

    }


    @PostMapping("feign/login")
    @RateLimiter(name = "user-service-instance", fallbackMethod = "myRatelimitFallback")
    @CircuitBreaker(name = "user-service-instance", fallbackMethod = "myCircuitFallback")
    public CommonResult login(@RequestBody Users user) {
        return userFeignApi.login(user);

    }

    //测试断路器 重试机制
    @CircuitBreaker(name = "test-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "test-service-instance", fallbackMethod = "myRatelimitFallback")
    @GetMapping(value = "feign/test/circuit/{id}")
    public String myCircuit(@PathVariable("id") Integer id) {
        return testFeignApi.myCircuit(id);
    }


    /**
     * 用户注册
     *
     * @param user
     * @return
     */

    @CircuitBreaker(name = "user-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "user-service-instance", fallbackMethod = "myRatelimitFallback")
    @PostMapping("feign/register")
    public CommonResult addUser(@RequestBody Users user) {
        return userFeignApi.addUser(user);

    }


    /**
     * 个人中心
     */
    @CircuitBreaker(name = "user-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "user-service-instance", fallbackMethod = "myRatelimitFallback")

    @GetMapping("feign/suthome/stuindex/myinfo")
    public CommonResult getInfo(@RequestParam(name = "uid") Integer uid) {
        return userFeignApi.getInfo(uid);

    }

    /**
     * 通过ID修改学生头像
     *
     * @return
     */
    @CircuitBreaker(name = "user-service-instance", fallbackMethod = "myCircuitFallback")
    @RateLimiter(name = "user-service-instance", fallbackMethod = "myRatelimitFallback")

    @PostMapping("feign/stu/editimg")
    public CommonResult stuEditImg(@RequestBody Users users) {
        return userFeignApi.stuEditImg(users);

    }

    /**
     * 修改密码
     */
    @CircuitBreaker(name = "user-service-instance", fallbackMethod = "myCircuitFallback")

    @PostMapping("feign/stu/edit/password")
    @RateLimiter(name = "user-service-instance", fallbackMethod = "myRatelimitFallback")
    public CommonResult stuEditPassword(@RequestBody EditPasswordVo editPasswordVo) {
        return userFeignApi.stuEditPassword(editPasswordVo);

    }

//    // 限流回退方法
//    public CommonResult myRatelimitFallback(Exception t) {
//        System.out.println("限流");
//        throw new BizException(ExceptionEnum.SERVICE_FLOW_RESTRICTION);
//    }


//    //断路器开启后其他访问的友情提示
//    public CommonResult myCircuitFallback(CallNotPermittedException t)   {
//        throw new BizException(ExceptionEnum.SERVICE_DEGRADATION);
//    }

    //断路器的友情提示
//    public CommonResult myCircuitFallback(RuntimeException t) {
//        System.out.println("RuntimeException 1");
//        throw new BizException(ExceptionEnum.INTERNAL_SERVER_ERROR);
//    }

    //断路器的友情提示
//    public CommonResult myCircuitFallback(Exception t) {
//        System.out.println("Exception 1");
//
//        throw new BizException(ExceptionEnum.SERVICE_DEGRADATION);
//    }


    //myCircuitFallback就是服务降级后的兜底处理方法
    //断路器关闭时遇到出错友情提示
//    public CommonResult myCircuitFallback(Throwable t)   {
//        // 这里是容错处理逻辑，返回备用结果
//        throw new BizException(ExceptionEnum.INTERNAL_SERVER_ERROR);
//    }


    //舱壁回退友情提醒
//    public CommonResult myBulkheadPoolFallback(Throwable t)   {
//        throw new BizException(ExceptionEnum.SERVER_BUSY);
//    }

}
