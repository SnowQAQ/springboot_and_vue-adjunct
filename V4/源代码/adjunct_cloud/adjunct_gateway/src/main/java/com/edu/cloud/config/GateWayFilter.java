package com.edu.cloud.config;

import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;


@Component
public class GateWayFilter implements GlobalFilter, Ordered {
    private static final List<String> EXCLUDED_PATHS = Arrays.asList(
            "/feign/login",
            "/feign/register",
            "/feign/webjars/**",
            "/feign/images/**",
            "/feign/static/**",
            "/feign/assets/**",
            "/stu/notes/download/**"
    );

    private boolean shouldSkip(String path) {
        // 检查路径是否在排除列表中
        for (String excludedPath : EXCLUDED_PATHS) {
            if (pathMatches(path, excludedPath)) {
                return true;
            }
        }
        return false;
    }

    private boolean pathMatches(String path, String pattern) {
        // 这里可以使用Ant风格的路径匹配或其他匹配方式
        // 这里只是一个简单的字符串或模式匹配示例
        if (pattern.endsWith("/**")) {
            String prefix = pattern.substring(0, pattern.length() - 3);
            return path.startsWith(prefix);
        }
        return path.equals(pattern);
    }
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
//        // 检查路径是否在排除列表中
        if (shouldSkip(exchange.getRequest().getPath().value())) {
            // 如果在排除列表中，直接执行下一个过滤器
            return chain.filter(exchange);
        }
        // 1.获取请求参数
        //1.这里的request并不是servlet中的request
        //2.返回值是一个多键的map集合、也就是说这个map集合的键可以重复
        MultiValueMap<String, String> headers = exchange.getRequest().getHeaders();
        // 2.获取请求头token参数
        String token = headers.getFirst("token");
        // 3.校验
        if (token != null) {
            boolean result = TokenUtil.verify(token);
            if (result){
                System.out.println("通过拦截器");
                return chain.filter(exchange);
            }
        }

        // Token验证失败，设置响应状态码和响应体
        ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        // 设置Content-Type为application/json
        response.getHeaders().add("Content-Type", "application/json");
        DataBufferFactory bufferFactory = response.bufferFactory();
        byte[] responseBodyBytes = "{\"msg\":\"token verify fail\",\"code\":\"50000\"}".getBytes(StandardCharsets.UTF_8);
        DataBuffer responseBuffer = bufferFactory.wrap(responseBodyBytes);

        return response.writeWith(Flux.just(responseBuffer))
                .then();
    }

    @Override
    public int getOrder() {
        return -1;
    }
}

