package com.edu.cloud.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;  
import org.springframework.context.annotation.Configuration;  
import javax.sql.DataSource;

/**
 *   datasource:
 *     url: jdbc:mysql://127.0.0.1:3306/concurrently?useUnicode=true&characterEncoding=utf-8
 *     username: root
 *     password: hsp
 *     driver-class-name: com.mysql.cj.jdbc.Driver
 */
@Configuration  
//@ConfigurationProperties(prefix = "spring.datasource")
public class DataSourceConfig {

    @Value("jdbc:mysql://127.0.0.1:3307/concurrently?useUnicode=true&characterEncoding=utf-8")
    private String url;
    @Value("root")
    private String username;
    @Value("1234")
    private String password;
    @Value("com.mysql.cj.jdbc.Driver")
    private String driverClassName;
  
    // 标准的getter和setter方法  
  
    @Bean  
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                .url(this.url)  
                .username(this.username)  
                .password(this.password)  
                .driverClassName(this.driverClassName)
                .build();  
    }  
}