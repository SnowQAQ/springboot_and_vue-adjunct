package com.edu.cloud.exp;

import com.atguigu.cloud.rspon.CommonResult;
import com.atguigu.cloud.rspon.ResultCode;
import feign.FeignException;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @description: 自定义异常处理
 * @author: DT
 * @date: 2021/4/19 21:51
 * @version: v1.0
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    
    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理自定义的业务异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = BizException.class)
    @ResponseBody
    public CommonResult bizExceptionHandler(HttpServletRequest req, BizException e){
        logger.error("发生业务异常！原因是：{}",e.getErrorMsg());
        return CommonResult.failed(Long.parseLong(e.getErrorCode()),e.getErrorMsg());
    }

    /**
     * NoResourceFoundException 微服务没有这个路径
     */


    /**
     * FeignException 404找不到微服务
     */
    @ExceptionHandler(value = FeignException.class)
    @ResponseBody
    public CommonResult exceptionHandler(HttpServletRequest req, FeignException e){
        logger.error("发生业务异常！原因是：{}",e.getMessage());
        return CommonResult.failed(ResultCode.INTERNAL_SERVER_ERROR);
    }

    /**
     *
     * 限流
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value = RequestNotPermitted.class)
    @ResponseBody
    public CommonResult exceptionHandler(HttpServletRequest req, RequestNotPermitted e){
        logger.error("发生业务异常！原因是：{}",e.getMessage());
        return CommonResult.failed(ResultCode.SERVICE_FLOW_RESTRICTION);
    }
    /**
     * 中断异常
     * InterruptedException
     */
    @ExceptionHandler(value =InterruptedException.class)
    @ResponseBody
    public CommonResult exceptionHandler(HttpServletRequest req, InterruptedException e){
        logger.error("发生中断异常！原因是:",e);
        return CommonResult.failed(ResultCode.INTERNAL_SERVER_ERROR);
    }
    /**
     * 断路器打开异常
     * CallNotPermittedException
     */
    @ExceptionHandler(value = CallNotPermittedException.class)
    @ResponseBody
    public CommonResult exceptionHandler(HttpServletRequest req, CallNotPermittedException e){
        logger.error("发生中断异常！原因是:",e);
        return CommonResult.failed(ResultCode.SERVICE_DEGRADATION);
    }


    /**
     * 处理空指针的异常
     * @param req
     * @param e
     * @return
     */
    @ExceptionHandler(value =NullPointerException.class)
    @ResponseBody
    public CommonResult exceptionHandler(HttpServletRequest req, NullPointerException e){
        logger.error("发生空指针异常！原因是:",e);
        return CommonResult.failed(ResultCode.INTERNAL_SERVER_ERROR);
    }


//    @ExceptionHandler(value =RuntimeException.class)
//    @ResponseBody
//    public CommonResult exceptionHandler(HttpServletRequest req, RuntimeException e){
//        logger.error("未知异常！原因是:",e);
//        return CommonResult.failed(ResultCode.INTERNAL_SERVER_ERROR);
//    }
//
//    /**
//     * 处理其他异常
//     * @param req
//     * @param e
//     * @return
//     */
//    @ExceptionHandler(value =Exception.class)
//    @ResponseBody
//    public CommonResult exceptionHandler(HttpServletRequest req, Exception e){
//        logger.error("未知异常！原因是:",e);
//        return CommonResult.failed(ResultCode.INTERNAL_SERVER_ERROR);
//    }
}
