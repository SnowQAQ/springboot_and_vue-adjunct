package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.TopMapper;
import com.edu.cloud.entiy.Top;
import com.edu.cloud.service.TopService;
import org.springframework.stereotype.Service;

/**
* @author 。
* @description 针对表【top】的数据库操作Service实现
* @createDate 2023-11-16 13:33:56
*/
@Service
public class TopServiceImpl extends ServiceImpl<TopMapper, Top>
    implements TopService {

}




