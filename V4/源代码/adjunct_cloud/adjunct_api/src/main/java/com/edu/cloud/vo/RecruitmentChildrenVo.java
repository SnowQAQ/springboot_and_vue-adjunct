package com.edu.cloud.vo;

import com.edu.cloud.entiy.Recruitment;
import lombok.Data;

import java.util.List;

@Data
public class RecruitmentChildrenVo {
    public RecruitmentChildrenVo(Integer id, List<Recruitment> children) {
        this.id = id;
        this.children = children;
    }

    Integer id;
    List<Recruitment> children;


}
