package com.edu.cloud.apis;


import com.atguigu.cloud.rspon.CommonResult;
import com.edu.cloud.entiy.*;
import com.edu.cloud.vo.BusinessShopAllInfoVo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(value = "root-employer-service-instance")
public interface RootEmployerFeignApi {

    /**
     * 商家管理列表
     * @return
     */
    @GetMapping("business/show")
    public CommonResult merchantsShow(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size);

    /**
     * 商店管理列表
     *
     * @return
     */
    @GetMapping("shop/show")
    public CommonResult shopShow(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size);


    /**
     * 招聘管理列表
     *
     * @return
     */
    @GetMapping("recruitment/show")
    public CommonResult recruitmentShow();



    /**
     * 通过ID修改商家信息
     *
     * @return
     */
    @PostMapping("business/edit")
    public CommonResult merchantsEditById(@RequestBody Users users);

    /**
     * 通过ID删除商家信息
     * 删除这商家的商店、招聘、简历管理、轮播图、头条、评价
     * @return
     */
    @GetMapping("business/del/{uid}")
    public CommonResult merchantsDeleteById(@PathVariable Integer uid);


    /**
     * 通过ID修改商店信息
     *
     * @return
     */
    @PostMapping("shop/edit")
    public CommonResult shopEditById(@RequestBody BusinessShopAllInfoVo businessShop);

    /**
     * 通过ID删除商店信息
     *
     * @return
     */
    @GetMapping("shop/del/{bid}")
    public CommonResult shopDeleteById(@PathVariable Integer bid);

    /**
     * 管理员通过nid删除某个招聘下的某个简历
     */
    @PostMapping("rec/notes/del")
    public CommonResult delNotesByNid(@RequestParam(name = "nId") Integer nId);


    /**
     * 获取所有招聘信息下的评论信息
     */
    @GetMapping("rec/comment")
    public CommonResult getAllRecAndComment();

}
