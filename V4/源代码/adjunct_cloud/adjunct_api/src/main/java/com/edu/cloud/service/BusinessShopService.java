package com.edu.cloud.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.Banner;
import com.edu.cloud.entiy.BusinessShop;
import com.edu.cloud.vo.BusinessShopAllInfoVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
* @author 。
* @description 针对表【business_shop】的数据库操作Service
* @createDate 2023-11-16 13:33:14
*/

public interface BusinessShopService extends IService<BusinessShop>  {
    List<BusinessShopAllInfoVo>  getBusinessShopInfoById(Integer id);

    IPage<BusinessShopAllInfoVo> getBusinessShopInfo(Page<BusinessShopAllInfoVo> page);

    IPage<BusinessShopAllInfoVo>  getBusinessShopInfoByLikeName(Page<BusinessShopAllInfoVo> page,String str);

    List<BusinessShopAllInfoVo> shopShow(Integer id);

    boolean shopAdd(BusinessShop businessShop) throws IOException;

    boolean shopEdit(BusinessShop businessShop);

    boolean shopDelete(Integer id);

//    List<Recruitment> getAllRecIdByMy();
}
