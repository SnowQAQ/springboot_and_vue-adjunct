package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.CommentMapper;
import com.edu.cloud.entiy.Comment;
import com.edu.cloud.service.CommentService;
import com.edu.cloud.vo.CommentAndChildrenVo;
import com.edu.cloud.vo.CommentChildrenCommentReply;
import com.edu.cloud.vo.CommentReplyAndUserInfoVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Snow
* @description 针对表【comment】的数据库操作Service实现
* @createDate 2023-11-27 21:47:32
*/
@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comment>
    implements CommentService {

    @Resource

    CommentMapper commentMapper;
    @Override
    public List<CommentAndChildrenVo> getCommentByRid(Integer rid) {
        return commentMapper.getCommentByRid(rid);
    }

    @Override
    public List<CommentReplyAndUserInfoVo> getCommentReplyByCid(Integer cid) {
        return commentMapper.getCommentReplyByCid(cid);
    }

    @Override
    public List<CommentChildrenCommentReply> getAllCommentAndReply() {
        return commentMapper.getAllCommentAndReply();
    }
}




