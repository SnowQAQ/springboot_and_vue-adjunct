package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.SResumeMapper;
import com.edu.cloud.entiy.SResume;
import com.edu.cloud.service.SResumeService;
import com.edu.cloud.vo.SResumeAndNid;
import com.edu.cloud.vo.SResumeAndUserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author 。
* @description 针对表【s_resume】的数据库操作Service实现
* @createDate 2023-11-16 13:33:45
*/
@Service
public class SResumeServiceImpl extends ServiceImpl<SResumeMapper, SResume>
    implements SResumeService {

    @Resource
    SResumeMapper sResumeMapper;
    @Override
    public Integer getNotesConstById(Integer uid) {
        return sResumeMapper.getNotesConstById(uid);
    }

    @Override
    public List<SResumeAndUserVo> getStuNotesAndUserInfo() {
        return sResumeMapper.getStuNotesAndUserInfo();
    }

    @Override
    public List<SResumeAndNid> getAllNotesByTid(Integer tid, Integer nid) {
        return sResumeMapper.getAllNotesByTid(tid,nid);
    }

    @Override
    public SResumeAndNid getOneTid(Integer sResumeQueryWrapper) {
        return sResumeMapper.getOneTid(sResumeQueryWrapper);
    }
}




