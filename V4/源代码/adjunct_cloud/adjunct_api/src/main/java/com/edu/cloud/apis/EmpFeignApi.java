package com.edu.cloud.apis;

import com.atguigu.cloud.rspon.CommonResult;
import com.edu.cloud.entiy.BusinessShop;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;


@FeignClient(value = "employer-service-instance")
public interface EmpFeignApi {
    /**
     * 通过商家id获取店铺的信息列表
     */
    @GetMapping("managehome/shop/{id}")
    public CommonResult shopShow(@PathVariable Integer id);

    /**
     * 添加商店信息
     */
    @PostMapping("managehome/shop/add")
    public CommonResult shopAdd(@RequestBody BusinessShop businessShop) throws IOException;


    /**
     * 修改商店信息
     */
    @PostMapping("managehome/shop/edit")
    public CommonResult shopEdit(@RequestBody BusinessShop businessShop);


    /**
     * 删除商店
     */
    @GetMapping("managehome/shop/delete/{id}")
    public CommonResult shopDelete(@PathVariable Integer id);
}
