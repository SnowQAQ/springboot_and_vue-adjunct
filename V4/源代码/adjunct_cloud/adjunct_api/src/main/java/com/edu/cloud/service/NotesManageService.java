package com.edu.cloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.NotesManage;
import com.edu.cloud.vo.RecruitmentAndSonNotesManageVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Snow
* @description 针对表【notes_manage】的数据库操作Service
* @createDate 2023-11-27 02:14:22
*/

public interface NotesManageService extends IService<NotesManage> {
    List<RecruitmentAndSonNotesManageVo> getNotesManageDistinctBossId(Integer bossId);


    Integer getMyNotesCount(Integer uid,Integer recid);
}
