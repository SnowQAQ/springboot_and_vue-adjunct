package com.edu.cloud.apis;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "test-service-instance")
public interface TestFeignApi {
    @GetMapping(value = "test/circuit/{id}")
    public String myCircuit(@PathVariable("id") Integer id);
}
