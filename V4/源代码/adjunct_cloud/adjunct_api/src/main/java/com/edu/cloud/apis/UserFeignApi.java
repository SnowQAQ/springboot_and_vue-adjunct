package com.edu.cloud.apis;

import com.atguigu.cloud.rspon.CommonResult;
import com.edu.cloud.entiy.Users;
import com.edu.cloud.vo.EditPasswordVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(value = "user-service-instance")
public interface UserFeignApi {



    @PostMapping("login")
    public CommonResult login(@RequestBody Users user);


    /**
     * 用户注册
     * @param user
     * @return
     */
    @PostMapping("register")
    public CommonResult addUser(@RequestBody Users user);


    /**
     * 个人中心
     */
    @GetMapping("suthome/stuindex/myinfo")
    public CommonResult getInfo(@RequestParam(name = "uid") Integer uid);

    /**
     * 通过ID修改学生头像
     *
     * @return
     */
    @PostMapping("stu/editimg")
    public CommonResult stuEditImg(@RequestBody Users users);

    /**
     * 修改密码
     */
    @PostMapping("stu/edit/password")
    public CommonResult stuEditPassword(@RequestBody EditPasswordVo editPasswordVo);
}
