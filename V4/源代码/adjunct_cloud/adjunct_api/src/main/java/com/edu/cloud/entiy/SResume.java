package com.edu.cloud.entiy;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * 
 * @TableName s_resume
 */
@TableName(value ="s_resume")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SResume implements Serializable {
    /**
     * 
     */
    @TableId(type = IdType.AUTO)
    private Integer tId;

    /**
     * 
     */
    private Integer uid;
    /**
     *
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 
     */
    private String filename;

    /**
     * 
     */
    private String filepath;

//    private Integer nId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}