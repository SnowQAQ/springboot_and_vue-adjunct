package com.edu.cloud.apis;

import com.atguigu.cloud.rspon.CommonResult;
import com.edu.cloud.entiy.Comment;
import com.edu.cloud.entiy.CommentReply;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "review-service-instance")
public interface ReviewFeignApi {
    /**
     * 通过简历id展现出对应的评论
     */
    @GetMapping("stu/rec/comment/{rid}")
    public CommonResult commentShowByRecId(@PathVariable Integer rid);

    /**
     * 增加一级评论
     */
    @PostMapping("stu/rec/add/comment")
    public CommonResult addComment(@RequestBody Comment comment);


    /**
     * 增加二级评论 前端把com_id存入answer_id 外键
     */
    @PostMapping("stu/rec/add/reply")
    public CommonResult addCommentReply(@RequestBody CommentReply comment);

    /**
     * 删除指定父级评论
     */
    @GetMapping("stu/rec/del/comment/{cid}")
    public CommonResult delComment(@PathVariable Integer cid);

    /**
     * 删除指定子级评论
     */
    @GetMapping("stu/rec/del/reply/{rid}")
    public CommonResult delCommentReply(@PathVariable Integer rid);
}
