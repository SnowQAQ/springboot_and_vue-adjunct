package com.edu.cloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.edu.cloud.entiy.CommentReply;
import org.apache.ibatis.annotations.Mapper;

/**
* @author Snow
* @description 针对表【comment_reply】的数据库操作Mapper
* @createDate 2023-11-27 21:47:32
* @Entity com.edu.boot.entity.CommentReply
*/
@Mapper
public interface CommentReplyMapper extends BaseMapper<CommentReply> {

}




