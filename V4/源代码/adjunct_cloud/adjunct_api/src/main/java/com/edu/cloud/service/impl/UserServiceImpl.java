package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.UserMapper;
import com.edu.cloud.entiy.Users;
import com.edu.cloud.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, Users> implements UserService {

    @Resource
    UserMapper userMapper;
    @Override
    public IPage<Users> selectUserByGender(Page<Users> page, String str){
        return userMapper.selectUserByGender(page,str);
    }

    @Override
    public IPage<Users> pageByGender(Page<Users> page, QueryWrapper<Users> queryWrapper) {
        return userMapper.pageSelect(page,queryWrapper);
    }
}
