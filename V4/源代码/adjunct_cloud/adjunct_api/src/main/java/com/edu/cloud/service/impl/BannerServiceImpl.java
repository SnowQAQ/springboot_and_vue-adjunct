package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.BannerMapper;
import com.edu.cloud.entiy.Banner;
import com.edu.cloud.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author 。
* @description 针对表【banner】的数据库操作Service实现
* @createDate 2023-11-16 13:31:23
*/
@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, Banner>
    implements BannerService {

    @Resource
    BannerMapper bannerMapper;
    @Override
    public List<Banner> getBannerTopList() {
        return bannerMapper.getBannerTopList();
    }
}




