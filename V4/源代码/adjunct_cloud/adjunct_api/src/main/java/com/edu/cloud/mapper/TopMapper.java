package com.edu.cloud.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.edu.cloud.entiy.Top;
import org.apache.ibatis.annotations.Mapper;

/**
* @author 。
* @description 针对表【top】的数据库操作Mapper
* @createDate 2023-11-16 13:33:56
* @Entity com.edu.boot.entity.Top
*/
@Mapper
public interface TopMapper extends BaseMapper<Top> {

}




