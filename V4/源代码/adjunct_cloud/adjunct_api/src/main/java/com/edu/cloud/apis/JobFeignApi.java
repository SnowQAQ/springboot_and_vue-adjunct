package com.edu.cloud.apis;


import com.edu.cloud.entiy.*;
import com.atguigu.cloud.rspon.CommonResult;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;


@FeignClient(value = "job-management-service-instance")
public interface JobFeignApi {
    /**
     * 通过店铺id获取招聘信息
     */
    
    @GetMapping("managehome/shop/recruitment/{id}")
    public CommonResult shopRecruitmentShow(@PathVariable Integer id) ;
    /**
     * 新增招聘信息
     */
    
    @PostMapping("managehome/shop/recruitment/add")
    public CommonResult addRecruitment(@RequestBody Recruitment recruitment) ;
    /**
     * 修改招聘信息
     */
    
    @PostMapping("managehome/shop/recruitment/edit")
    public CommonResult editRecruitment(@RequestBody Recruitment recruitment);


    /**
     * 删除招聘信息 recId
     * 删除他的头条 和 轮播图 和 收到的简历、评论 再删除这招聘
     */
    
    @GetMapping("managehome/shop/recruitment/del/{id}")
    public CommonResult delRecruitmentShow(@PathVariable Integer id);
    /**
     * 海报标题操作
     */
    
    @GetMapping("recruitment/poster/exist/{rid}")
    public CommonResult RecruitmentPosterExist(@PathVariable Integer rid);

    
    @PostMapping ("recruitment/poster/add")
    public CommonResult RecruitmentPoster(@RequestBody Banner banner);


    /**
     * 商家获取学生投递的简历 nId uid tId recId
     * 1.商家通过招聘信息来划分
     * 2.查看所有我的bossid 获取到我的所有招聘recid，查看这个招聘有没有学生发简历过来
     */
    
    @GetMapping("managehome/rec/notes/{id}")
    public CommonResult getMyAllNotes(@PathVariable Integer id);


    /**
     * 获取所有简历 以及 主人-商店详细
     * @return
     */
    
    @GetMapping("suthome/stuindex/getrec")
    public CommonResult getRec();

    /**
     * 通过简历rid获取所有这个简历 主人-商店详细信息
     */
    
    @GetMapping("suthome/stuindex/recshow/{id}")
    public CommonResult getRecInfoByid(@PathVariable Integer id);

    /**
     * 前端点击访问某个招聘页面，就给这个招聘信息热度+1
     */
    
    @GetMapping("suthome/stuindex/addheat/{rid}")
    public CommonResult AddRecHeat(@PathVariable Integer rid) ;

    /**
     * 前端轮播图 头条数据获取
     */
    
    @GetMapping("suthome/stuindex/bannertop")
    public CommonResult getTop();

}
