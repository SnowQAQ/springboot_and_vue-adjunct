package com.edu.cloud.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.Users;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;



public interface UserService extends IService<Users> {
    public IPage<Users> selectUserByGender(Page<Users> page, String str);

    public IPage<Users> pageByGender(Page<Users> page, QueryWrapper<Users> queryWrapper);
}
