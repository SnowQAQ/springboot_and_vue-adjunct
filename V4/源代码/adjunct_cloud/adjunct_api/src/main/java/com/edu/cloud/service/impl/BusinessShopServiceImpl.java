package com.edu.cloud.service.impl;

import com.atguigu.cloud.rspon.CommonResult;
import com.atguigu.cloud.rspon.ReturnCode;
import com.atguigu.cloud.tool.Base64Utils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.BusinessShopMapper;
import com.edu.cloud.entiy.BusinessShop;
import com.edu.cloud.service.BusinessShopService;
import com.edu.cloud.vo.BusinessShopAllInfoVo;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
* @author 。
* @description 针对表【business_shop】的数据库操作Service实现
* @createDate 2023-11-16 13:33:14
*/
@Service
public class BusinessShopServiceImpl extends ServiceImpl<BusinessShopMapper, BusinessShop>
    implements BusinessShopService {
    @Resource
    private BusinessShopMapper businessShopMapper;

    public List<BusinessShopAllInfoVo> getBusinessShopInfoById(Integer id){
        return businessShopMapper.getBusinessShopInfoById(id);
    }
    public IPage<BusinessShopAllInfoVo> getBusinessShopInfoByLikeName(Page<BusinessShopAllInfoVo> page,String str){
        return businessShopMapper.getBusinessShopInfoByLikeName(page,str);
    }

    @Override
    public List<BusinessShopAllInfoVo> shopShow(Integer id) {
        List<BusinessShopAllInfoVo> list = getBusinessShopInfoById(id);
        return list;
    }
    String defaultImgUser = ReturnCode.DEFAULT_IMG_USER;

    @Override
    public boolean shopAdd(BusinessShop businessShop) throws IOException {

        if (businessShop.getBImage() == null) {
            ClassPathResource readFile = new ClassPathResource(defaultImgUser);
            File file = readFile.getFile();
            MultipartFile cMultiFile = new MockMultipartFile(file.getName(), file.getName(), null, new FileInputStream(file));
            String base64img = Base64Utils.convertToBase64(cMultiFile);
            businessShop.setBImage(base64img);
        }
        return save(businessShop);
    }

    @Override
    public boolean shopEdit(BusinessShop businessShop) {
        UpdateWrapper<BusinessShop> usersUpdateWrapper = new UpdateWrapper<>();
        usersUpdateWrapper.eq("b_id", businessShop.getBId());
        usersUpdateWrapper.set("b_name", businessShop.getBName());
        usersUpdateWrapper.set("b_image", businessShop.getBImage());
        usersUpdateWrapper.set("b_head", businessShop.getBHead());
        usersUpdateWrapper.set("b_address", businessShop.getBAddress());
        boolean update = update(businessShop, usersUpdateWrapper);
        return update;
    }

    @Override
    public boolean shopDelete(Integer id) {
        QueryWrapper<BusinessShop> businessShopQueryWrapper = new QueryWrapper<>();
        businessShopQueryWrapper.eq("b_id", id);
        boolean remove = remove(businessShopQueryWrapper);
        return remove;
    }

    public IPage<BusinessShopAllInfoVo> getBusinessShopInfo(Page<BusinessShopAllInfoVo> page){
        return businessShopMapper.getBusinessShopInfo(page);
    }
}




