package com.edu.cloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.CommentReply;
import org.springframework.context.annotation.Lazy;

/**
* @author Snow
* @description 针对表【comment_reply】的数据库操作Service
* @createDate 2023-11-27 21:47:32
*/

public interface CommentReplyService extends IService<CommentReply> {

}
