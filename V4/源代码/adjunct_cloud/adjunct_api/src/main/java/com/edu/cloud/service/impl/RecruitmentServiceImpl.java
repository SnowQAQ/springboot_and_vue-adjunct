package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.RecruitmentMapper;
import com.edu.cloud.entiy.Recruitment;
import com.edu.cloud.service.RecruitmentService;
import com.edu.cloud.vo.RecruitmentAllInfoVo;
import com.edu.cloud.vo.ShopAndRecruitmentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author 。
* @description 针对表【recruitment】的数据库操作Service实现
* @createDate 2023-11-16 13:53:20
*/
@Service
public class RecruitmentServiceImpl extends ServiceImpl<RecruitmentMapper, Recruitment>
    implements RecruitmentService {

    @Resource

    RecruitmentMapper recruitmentMapper;

    @Override
    public List<RecruitmentAllInfoVo> getRecruitmentAndInfoById(Integer uid, Integer bid) {
        return recruitmentMapper.getRecruitmentAndInfoById(uid,bid);
    }

    @Override
    public List<Recruitment> getRecruitmentById(Integer id) {
        return recruitmentMapper.getRecruitmentById(id);
    }

    @Override
    public List<ShopAndRecruitmentVo> getShopAndRecruitmentInfoByBid(Integer bid){
        return recruitmentMapper.getShopAndRecruitmentInfoByBid(bid);
    }

    @Override
    public RecruitmentAllInfoVo getUserAllShopRecruitmentInfoByRid(Integer rid) {
        return recruitmentMapper.getUserAllShopRecruitmentInfoByRid(rid);
    }

    @Override
    public List<RecruitmentAllInfoVo> getUserAllInfo(){
        return recruitmentMapper.getUserAllInfo();
    }

    @Override
    public List<RecruitmentAllInfoVo> getUserAllInfoSearch(String search) {
        return recruitmentMapper.getUserAllInfoSearch(search);
    }

    @Override
    public boolean addRecruitment(Recruitment recruitment) {
        return save(recruitment);

    }

    @Override
    public boolean editRecruitment(Recruitment recruitment) {
        UpdateWrapper<Recruitment> recruitmentUpdateWrapper = new UpdateWrapper<>();

        recruitmentUpdateWrapper.eq("rec_id", recruitment.getRecId());
        recruitmentUpdateWrapper.set("rec_salary",recruitment.getRecSalary());
        recruitmentUpdateWrapper.set("rec_station",recruitment.getRecStation());
        recruitmentUpdateWrapper.set("rec_info",recruitment.getRecInfo());
        recruitmentUpdateWrapper.set("rec_obj",recruitment.getRecObj());

        return update(recruitmentUpdateWrapper);
    }
}




