package com.edu.cloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.Top;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
* @author 。
* @description 针对表【top】的数据库操作Service
* @createDate 2023-11-16 13:33:56
*/

public interface TopService extends IService<Top> {

}
