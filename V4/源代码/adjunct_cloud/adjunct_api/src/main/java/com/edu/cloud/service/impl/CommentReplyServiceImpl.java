package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.CommentReplyMapper;
import com.edu.cloud.entiy.CommentReply;
import com.edu.cloud.service.CommentReplyService;
import org.springframework.stereotype.Service;

/**
* @author Snow
* @description 针对表【comment_reply】的数据库操作Service实现
* @createDate 2023-11-27 21:47:32
*/
@Service
public class CommentReplyServiceImpl extends ServiceImpl<CommentReplyMapper, CommentReply>
    implements CommentReplyService {

}




