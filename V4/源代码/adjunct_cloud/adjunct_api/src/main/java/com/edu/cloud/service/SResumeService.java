package com.edu.cloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.SResume;
import com.edu.cloud.vo.SResumeAndNid;
import com.edu.cloud.vo.SResumeAndUserVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 。
* @description 针对表【s_resume】的数据库操作Service
* @createDate 2023-11-16 13:33:45
*/

public interface SResumeService extends IService<SResume> {

    Integer getNotesConstById(Integer uid);

    List<SResumeAndUserVo> getStuNotesAndUserInfo();

    List<SResumeAndNid> getAllNotesByTid(Integer tid, Integer nid);

    SResumeAndNid getOneTid(Integer sResumeQueryWrapper);
}
