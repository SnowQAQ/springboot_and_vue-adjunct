package com.edu.cloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.Comment;
import com.edu.cloud.vo.CommentAndChildrenVo;
import com.edu.cloud.vo.CommentChildrenCommentReply;
import com.edu.cloud.vo.CommentReplyAndUserInfoVo;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author Snow
* @description 针对表【comment】的数据库操作Service
* @createDate 2023-11-27 21:47:32
*/


public interface CommentService extends IService<Comment> {

    List<CommentAndChildrenVo> getCommentByRid(Integer rid);
    List<CommentReplyAndUserInfoVo> getCommentReplyByCid(Integer cid);

    List<CommentChildrenCommentReply> getAllCommentAndReply();
}

