package com.edu.cloud.vo;

import lombok.Data;

@Data
public class EditPasswordVo {
    private Integer uid;
    private String oldPassword;
    private String password;

}
