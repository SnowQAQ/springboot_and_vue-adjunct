package com.edu.cloud.apis;


import com.atguigu.cloud.rspon.CommonResult;
import com.edu.cloud.entiy.Users;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(value = "root-students-service-instance")
public interface RootStudentsFeignApi {
    /**
     * 学生管理列表
     * @return
     */
    @GetMapping("stu/show")
    public CommonResult stuShow(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size);

    /**
     * 通过ID修改学生信息
     * @return
     */
    @PostMapping("stu/edit")
    public CommonResult stuEditById(@RequestBody Users users);

    /**
     * 通过ID删除学生信息
     * @return
     */
    @PostMapping("stu/delete")
    public CommonResult stuDeleteById(@RequestBody Users users);
}
