package com.edu.cloud.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.edu.cloud.mapper.NotesManageMapper;
import com.edu.cloud.entiy.NotesManage;
import com.edu.cloud.service.NotesManageService;
import com.edu.cloud.vo.RecruitmentAndSonNotesManageVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Snow
* @description 针对表【notes_manage】的数据库操作Service实现
* @createDate 2023-11-27 02:14:22
*/
@Service
public class NotesManageServiceImpl extends ServiceImpl<NotesManageMapper, NotesManage>
    implements NotesManageService {

    @Resource
    NotesManageMapper notesManageMapper;
    @Override
    public List<RecruitmentAndSonNotesManageVo> getNotesManageDistinctBossId(Integer bossId) {
        return notesManageMapper.getNotesManageDistinctBossId(bossId);
    }

    @Override
    public Integer getMyNotesCount(Integer uid,Integer recid) {
        return notesManageMapper.getMyNotesCount(uid,recid);
    }
}




