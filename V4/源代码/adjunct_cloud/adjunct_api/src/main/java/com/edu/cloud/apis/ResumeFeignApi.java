package com.edu.cloud.apis;

import com.atguigu.cloud.rspon.CommonResult;
import com.edu.cloud.entiy.NotesManage;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;

@FeignClient(value = "resume-service-instance")
public interface ResumeFeignApi {

    /**
     * 商家点击简历查看需要改变简历状态码 通过nid
     */
    @GetMapping("notes/change/state/{nid}")
    public CommonResult changeState(@PathVariable Integer nid);

    /**
     * 简历通过功能
     * 商家添加通知信息、更改状态码为已通过状态
     */
    @PostMapping("managehome/notes/change")
    public CommonResult changeNotesInfoAndState(@RequestBody NotesManage notesManage);

    /**
     * 管理学生简历
     * @return
     */
    @GetMapping("stu/notes/manage")
    public CommonResult sResumeManage();
    /**
     * 上传简历
     */
    @PostMapping(value = "feign/stu/notes/upload/{uid}", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public CommonResult upload(@PathVariable Integer uid,@RequestPart MultipartFile file) throws IOException;
    /**
     * 获取简历
     */
    @GetMapping("stu/notes/{uid}")
    public CommonResult getNotes(@PathVariable Integer uid);
    /**
     * 下载简历
     */
    @GetMapping("stu/notes/download/{fileName}")
    public CommonResult download(@PathVariable String fileName, HttpServletResponse response) throws IOException;


    /**
     * 删除简历
     */
    @PostMapping("stu/notes/del")
    public CommonResult delNotes(@RequestParam(name = "uid") Integer uid,@RequestParam(name = "fileName") String fileName);



    /**
     * 投递简历
     * 直接存储这学生的uid、简历t_id、某个招聘rec_id
     */
    @PostMapping("stu/notes/send")
    public CommonResult sendNotes(@RequestBody NotesManage notesManage);

    /**
     * 通过学生id获取自己的投递信息
     * 返回某个招聘下的投递 以及状态码、通知
     */
    @GetMapping("stu/deliver/{uid}")
    public CommonResult getMyDeliver(@PathVariable Integer uid);
}
