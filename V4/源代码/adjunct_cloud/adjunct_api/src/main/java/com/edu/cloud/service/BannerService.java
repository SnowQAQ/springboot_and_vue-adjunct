package com.edu.cloud.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.edu.cloud.entiy.Banner;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author 。
* @description 针对表【banner】的数据库操作Service
* @createDate 2023-11-16 13:31:23
*/

public interface BannerService extends IService<Banner> {
    List<Banner> getBannerTopList();
}
