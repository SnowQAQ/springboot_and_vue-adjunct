package com.edu.cloud.apis;


import com.atguigu.cloud.rspon.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(value = "search-service-instance")
public interface SearchFeignApi {
    /**
     * 通过用户名搜索数据
     * @return
     */
    @GetMapping("stu/search")
    public CommonResult stuSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search);

    /**
     * 商家用户名搜索
     */
    @GetMapping("business/search")
    public CommonResult merchantsSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search);


    /**
     * 商店名字搜索
     */
    @GetMapping("shop/search")
    public CommonResult shopSearchByName(@RequestParam(name = "currentpage") Integer current, @RequestParam(name = "pagesize") Integer size, @RequestParam(name = "search") String search);
    /**
     * 搜索职位关键字返回前端 所有信息
     */
    @GetMapping("suthome/stuindex/search/{search}")
    public CommonResult searchRecInfo(@PathVariable String search);
}
