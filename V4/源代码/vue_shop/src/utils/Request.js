import axios from 'axios';
// 导入nprogress包和样式,用于进度条显示
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import { Message } from 'element-ui';
import router from '@/router';
router.push({});

// 重新创建一个带根节点的axios对象
const request = axios.create({
	// baseURL: 'http://10.200.88.199:8888/api/v1',
	// baseURL: 'http://10.96.38.60:8888/api/v1',
	baseURL: 'http://localhost:8888/feign',
	withCredentials: true,

});

// 拦截器（英文：Interceptors）会在每次发起 ajax 请求和得到响应的时候自动被触发
// 每次API操作都要拦截器带token令牌
request.interceptors.request.use((config) => {
	config.headers.token = sessionStorage.getItem('token');
	// 在request拦截器中，展示进度条NProgress.start()
	NProgress.start();

	return config;
});

request.interceptors.response.use((response) => {
	if (response.code == '50000') {
		Message.error(response.msg);
		sessionStorage.removeItem('token');
		// 重新跳转到login页面
		router.push('/login');
	}

	// 在response拦截器中，隐藏进度条NProgress.done()
	NProgress.done();

	return response;
},(error)=>{
	console.log(error.response);
	if (error.response.data.code == '50000') {
		Message.error("token失效，请重新登录！！");
		sessionStorage.removeItem('token');
		// 重新跳转到login页面
		router.push('/login');
	}
});
export default request;
