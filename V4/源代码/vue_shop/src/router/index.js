import Vue from 'vue';
import VueRouter from 'vue-router';

// 懒加载方式导入组件
const Login = () => import(/* webpackChunkName:"Login" */ '@/components/Login');
const Home = () => import(/* webpackChunkName:"Home" */ '@/components/Home');
const Wecome = () => import(/* webpackChunkName:"Wecome" */ '@/view/Wecome');
const StuManage = () => import(/* webpackChunkName:"StuManage" */ '@/view/root/StuManage');
const StuNotes = () => import(/* webpackChunkName:"StuNotes" */ '@/view/root/StuNotes');
const StuHome = () => import(/* webpackChunkName:"StuHome" */ '@/components/StuHome');
const ManageHome = () => import(/* webpackChunkName:"ManageHome" */ '@/components/ManageHome');
const StuIndex = () => import(/* webpackChunkName:"StuIndex" */ '@/view/StuIndex');
const ManageIndex = () => import(/* webpackChunkName:"ManageIndex" */ '@/view/ManageIndex');
const MyShop = () => import(/* webpackChunkName:"MyShop" */ '@/view/shop/MyShop');
const ShopChat = () => import(/* webpackChunkName:"ShopChat" */ '@/view/shop/ShopChat');
const ShopManage = () => import(/* webpackChunkName:"ShopManage" */ '@/view/shop/ShopManage');
const NotesManage = () => import(/* webpackChunkName:"NotesManage" */ '@/view/shop/NotesManage');
const OneRecruitment = () => import(/* webpackChunkName:"OneRecruitment" */ '@/view/shop/Recruitment');
const MerchantsManage = () => import(/* webpackChunkName:"MerchantsManage" */ '@/view/root/MerchantsManage');
const EvaluateManage = () => import(/* webpackChunkName:"EvaluateManage" */ '@/view/root/EvaluateManage');
const RootShopManage = () => import(/* webpackChunkName:"RootShopManage" */ '@/view/root/ShopManage');
const StuRecShow = () => import(/* webpackChunkName:"StuRecShow" */ '@/view/student/StuRecShow');
const StuMySelfInfo = () => import(/* webpackChunkName:"StuMySelfInfo" */ '@/view/student/StuMySelfInfo');
const StuMySelfNotes = () => import(/* webpackChunkName:"StuMySelfNotes" */ '@/view/student/StuMySelfNotes');
const StuDeliver = () => import(/* webpackChunkName:"StuDeliver" */ '@/view/student/StuDeliver');
Vue.use(VueRouter);

const routes = [
	{ path: '/', redirect: '/login' },
	{ path: '/login', component: Login },
	{
		path: '/home',
		component: Home,
		children: [
			{ path: '', redirect: 'wecome' },
			{ path: 'wecome', component: Wecome },
			{ path: 'stu/show', component: StuManage },
			{ path: 'stu/notes', component: StuNotes },
			{ path: 'business/show', component: MerchantsManage },
			{ path: 'shop/show', component: RootShopManage },
			{ path: 'evaluate/show', component: EvaluateManage },
		],
	},
	{
		path: '/suthome',
		component: StuHome,
		children: [
			{ path: '', redirect: 'stuindex' },
			{ path: 'stuindex', component: StuIndex },
			{ path: 'chat', name: 'Im', component: () => import('@/components/Im') },
			{ path: 'myself/info', component: StuMySelfInfo },
			{ path: 'myself/notes', component: StuMySelfNotes },
			{ path: 'deliver', component: StuDeliver },
			{ path: 'recshow/:rid', component: StuRecShow },
		],
	},
	{
		path: '/managehome',
		component: ManageHome,
		children: [
			{ path: '', redirect: 'manageindex' },
			{ path: 'manageindex', name: 'manageindex', component: ManageIndex },
			{ path: 'shop', component: ShopManage },
			{ path: 'notes', component: NotesManage },
			{ path: 'chat', name: 'Im', component: () => import('@/components/Im') },
			{ path: 'myshop', component: MyShop },
			{ path: 'shop/recruitment', component: OneRecruitment },
		],
	},

];

const router = new VueRouter({
	routes,
});

// 路由前置守卫
router.beforeEach((to, from, next) => {
	const tokenStr = sessionStorage.getItem('token');
	
	if (to.path === '') {
	  console.log(to.path);
	//   console.log(tokenStr);
	  // 如果有token就直接放行 没有就回去登录

	  if (tokenStr !== null) {
	    next()
	  } else {
	    next('/login')
	  }
	} else {
		console.log(to.path);
	  // 其他导航正常放行
	  next()
	}
});

export default router;
